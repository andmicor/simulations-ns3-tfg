/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

// Network topology
//
//       n0    n1
//       |     |
//       =======
//         LAN
//
// - UDP flows from n0 to n1

#include <fstream>
#include "ns3/core-module.h"
#include "ns3/csma-module.h"
#include "ns3/applications-module.h"
#include "ns3/internet-module.h"

#include <iostream>
#include "ns3/wifi-module.h"
#include "ns3/yans-wifi-helper.h"
#include "ns3/ocb-wifi-mac.h"
#include "ns3/wifi-80211p-helper.h"
#include "ns3/wave-mac-helper.h"
#include "ns3/mobility-module.h"

using namespace ns3;
using namespace std;

NS_LOG_COMPONENT_DEFINE ("WifiSimpleVanetTest-13-10");

int
main (int argc, char *argv[])
{
//
// Enable logging for UdpClient and server
//
// LogComponentEnable ("UdpTraceClient", LOG_LEVEL_INFO);
  LogComponentEnable ("UdpClient", LOG_LEVEL_INFO);
  LogComponentEnable ("UdpServer", LOG_LEVEL_INFO);
  //LogComponentEnable ("UdpSocketImpl", LOG_LEVEL_ALL);


  bool verbose = false;
  bool printPosition = true;
  double distance = 50;  // m
  Address serverAddress;
  double interval = 0.01; //second

  CommandLine cmd;
  cmd.Parse (argc, argv);
  cmd.AddValue ("interval", "UDP client packet interval", interval);

  std::string phyMode ("OfdmRate6MbpsBW10MHz");

//
// Explicitly create the nodes required by the topology (shown above).
//
  NS_LOG_INFO ("Create nodes.");
  NodeContainer container;
  container.Create (3);

  InternetStackHelper internet;
  internet.Install (container);

  NS_LOG_INFO ("Create channels.");
  //***************************************************************************

  // The below set of helpers will help us to put together the wifi NICs we want
  YansWifiPhyHelper wifiPhy =  YansWifiPhyHelper::Default ();
  YansWifiChannelHelper wifiChannel = YansWifiChannelHelper::Default ();
  Ptr<YansWifiChannel> channel = wifiChannel.Create ();
  wifiPhy.SetChannel (channel);
  // ns-3 supports generate a pcap trace
  wifiPhy.SetPcapDataLinkType (YansWifiPhyHelper::DLT_IEEE802_11);

  double maxPower = 30;
  wifiPhy.Set("TxPowerStart", DoubleValue(maxPower));
  wifiPhy.Set("TxPowerEnd", DoubleValue(maxPower));
  //wifiPhy.Set("TxPowerLevels", UintegerValue(1));
  //wifiPhy.Set("TxPowerStart", DoubleValue(33));

  NqosWaveMacHelper wifi80211pMac = NqosWaveMacHelper::Default ();
  Wifi80211pHelper wifi80211p = Wifi80211pHelper::Default ();
  if (verbose)
    {
      wifi80211p.EnableLogComponents ();      // Turn on all Wifi 802.11p logging
    }

  wifi80211p.SetRemoteStationManager ("ns3::ConstantRateWifiManager",
                                      "DataMode",StringValue (phyMode),
                                      "ControlMode",StringValue (phyMode));
  NetDeviceContainer devices = wifi80211p.Install (wifiPhy, wifi80211pMac, container);


//***************************************************************************************
// add IPs to the nodes

// We've got the "hardware" in place.  Now we need to add IP addresses.
//
  NS_LOG_INFO ("Assign IP Addresses.");
      Ipv4AddressHelper ipv4;
      ipv4.SetBase ("10.1.1.0", "255.255.255.0");
      Ipv4InterfaceContainer i = ipv4.Assign (devices);
      serverAddress = Address (i.GetAddress (1));


  NS_LOG_INFO ("Create Applications.");
//
// Create one udpServer applications on node one.
//
  uint16_t port = 9;
  UdpServerHelper server (port);
  ApplicationContainer appsServer = server.Install (container.Get (1));
  appsServer.Start (Seconds (1.0));
  appsServer.Stop (Seconds (20.0));

  //*******************************************************************************

//
// Create UdpClient application to send broadcast UDP messages
//



/*  UdpTraceClientHelper client (Ipv4Address("255.255.255.255"), port,"");
  ApplicationContainer appsClient;
  appsClient = client.Install (NodeContainer(container.Get (0), container.Get (2)));
    appsClient.Start (Seconds (2.0));
  	appsClient.Stop (Seconds (10.0));*/


  uint32_t MaxPacketSize = 500;
  Time interPacketInterval = Seconds (interval);

  UdpClientHelper client1 (Ipv4Address("255.255.255.255"), port);

  client1.SetAttribute ("PacketSize", UintegerValue (MaxPacketSize));
  client1.SetAttribute ("Interval", TimeValue (interPacketInterval));


  ApplicationContainer appsClient;
  appsClient = client1.Install (container.Get (0));
  appsClient.Start (Seconds (2.0));
  appsClient.Stop (Seconds (4.0));

//  appsClient = client1.Install (container.Get (2));
  appsClient.Start (Seconds (2.123));
  appsClient.Stop (Seconds (4.0));
  //appsClient = client1.Install (NodeContainer(container.Get (0)));






//*****************************************************************************************
// add mobility for the nodes

  MobilityHelper mobility;
      mobility.SetPositionAllocator ("ns3::GridPositionAllocator",
                                     "MinX", DoubleValue (0.0),
                                     "MinY", DoubleValue (0.0),
                                     "DeltaX", DoubleValue (distance),
                                     "DeltaY", DoubleValue (distance),
                                     "GridWidth", UintegerValue (5),
                                     "LayoutType", StringValue ("RowFirst"));
      mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
      mobility.Install (container);


  if(printPosition){
   // iterate our nodes and print their position.
 	for (NodeContainer::Iterator j = container.Begin ();
 		j != container.End (); ++j)
 		{
 		Ptr<Node> object = *j;
 		Ptr<MobilityModel> position = object->GetObject<MobilityModel> ();
 		NS_ASSERT (position != 0);
 		Vector pos = position->GetPosition ();
 		std::cout <<"Node ID: "<<object->GetId() <<", x=" << pos.x << ", y=" << pos.y << ", z=" << pos.z << std::endl;

 	}
 	cout<<"****************************************************"<<endl;
   }
  wifiPhy.EnablePcapAll ("code");
//
// Now, do the actual simulation.
//
  NS_LOG_INFO ("Run Simulation.");
  Simulator::Run ();
  Simulator::Destroy ();
  NS_LOG_INFO ("Done.");
}

