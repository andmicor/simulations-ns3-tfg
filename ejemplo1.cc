    /* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
    /*
     * This program is free software; you can redistribute it and/or modify
     * it under the terms of the GNU General Public License version 2 as
     * published by the Free Software Foundation;
     *
     * This program is distributed in the hope that it will be useful,
     * but WITHOUT ANY WARRANTY; without even the implied warranty of
     * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     * GNU General Public License for more details.
     *
     * You should have received a copy of the GNU General Public License
     * along with this program; if not, write to the Free Software
     * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
     */

    #include "ns3/core-module.h"
    #include "ns3/point-to-point-module.h"
    #include "ns3/network-module.h"
    #include "ns3/applications-module.h"
    #include "ns3/wifi-module.h"
    #include "ns3/mobility-module.h"
    #include "ns3/csma-module.h"
    #include "ns3/internet-module.h"
    #include "ns3/netanim-module.h"

    // Default Network Topology
    //
    //   Wifi 10.1.3.0
    //                 AP
    //  *    *    *    *
    //  |    |    |    |    10.1.1.0
    // n5   n6   n7   n0 -------------- n1   n2   n3   n4
    //                   point-to-point  |    |    |    |
    //                                   ================
    //                                     LAN 10.1.2.0

    using namespace ns3;

    NS_LOG_COMPONENT_DEFINE ("ThirdScriptExample");

    int 
    main (int argc, char *argv[])
    {
      bool verbose = true;
      uint32_t nCsma = 3; //Numero de elementos que se encuentran en la red LAN
      uint32_t nWifi = 3; //Numero de elementos que trabajan en la red WiFi
      bool tracing = false;

      CommandLine cmd;
      cmd.AddValue ("nCsma", "Number of \"extra\" CSMA nodes/devices", nCsma);
      cmd.AddValue ("nWifi", "Number of wifi STA devices", nWifi);
      cmd.AddValue ("verbose", "Tell echo applications to log if true", verbose);
      cmd.AddValue ("tracing", "Enable pcap tracing", tracing);

      cmd.Parse (argc,argv);

      // The underlying restriction of 18 is due to the grid position
      // allocator's configuration; the grid layout will exceed the
      // bounding box if more than 18 nodes are provided.
      if (nWifi > 18)
        {
          std::cout << "nWifi should be 18 or less; otherwise grid layout exceeds the bounding box" << std::endl;
          return 1;
        }

      if (verbose)
        {
          LogComponentEnable ("UdpEchoClientApplication", LOG_LEVEL_INFO);
          LogComponentEnable ("UdpEchoServerApplication", LOG_LEVEL_INFO);
        }

    //Se crean los nodos Punto a punto, en este caso son 2 (n0 y n1)
      NodeContainer p2pNodes;
      p2pNodes.Create (2);

    //Características del canal punto a punto
      PointToPointHelper pointToPoint;
      pointToPoint.SetDeviceAttribute ("DataRate", StringValue ("5Mbps"));
      pointToPoint.SetChannelAttribute ("Delay", StringValue ("2ms"));

    //Se instala la Topología Punto a Punto
      NetDeviceContainer p2pDevices;
      p2pDevices = pointToPoint.Install (p2pNodes);

    //Se propone la red LAN, se añaden los 3 dispositivos + el n1 que es perteneciente a la Top. Punto a punto
      NodeContainer csmaNodes;
      csmaNodes.Add (p2pNodes.Get (1));//Se añade n1
      csmaNodes.Create (nCsma);  //Se añaden los demás nodos

    //Se configura el canal de LAN que es CSMA
      CsmaHelper csma;
      csma.SetChannelAttribute ("DataRate", StringValue ("100Mbps"));
      csma.SetChannelAttribute ("Delay", TimeValue (NanoSeconds (6560)));

    //Se instala la LAN
      NetDeviceContainer csmaDevices;
      csmaDevices = csma.Install (csmaNodes);

    //Se configura la WiFi
      NodeContainer wifiStaNodes; //Estáticos
      wifiStaNodes.Create (nWifi); //Se crean 3 nodos para WiFi
      NodeContainer wifiApNode = p2pNodes.Get (0); //El nodo 0 es el AP, que se añade a la Topología WiFi

    //Se configura el canal para el WiFi 802.11
      YansWifiChannelHelper channel = YansWifiChannelHelper::Default ();
      YansWifiPhyHelper phy = YansWifiPhyHelper::Default ();
      phy.SetChannel (channel.Create ());

    //Configuraciones varias WiFi
      WifiHelper wifi;
      wifi.SetRemoteStationManager ("ns3::AarfWifiManager");

    //Configuraciones varias WiFi
      WifiMacHelper mac;
      Ssid ssid = Ssid ("ns-3-ssid");
      mac.SetType ("ns3::StaWifiMac",
                   "Ssid", SsidValue (ssid),
                   "ActiveProbing", BooleanValue (false));

    //Configuraciones varias WiFi
      NetDeviceContainer staDevices;
      staDevices = wifi.Install (phy, mac, wifiStaNodes);

    //Configuraciones varias WiFi
      mac.SetType ("ns3::ApWifiMac",
                   "Ssid", SsidValue (ssid));

    //Configuraciones varias WiFi
      NetDeviceContainer apDevices;
      apDevices = wifi.Install (phy, mac, wifiApNode);

    //Mobilidad de los dispositivos que se encuentran en la red WiFi
      MobilityHelper mobility;

      mobility.SetPositionAllocator ("ns3::GridPositionAllocator",
                                     "MinX", DoubleValue (0.0),
                                     "MinY", DoubleValue (0.0),
                                     "DeltaX", DoubleValue (5.0),
                                     "DeltaY", DoubleValue (10.0),
                                     "GridWidth", UintegerValue (3),
                                     "LayoutType", StringValue ("RowFirst"));

      mobility.SetMobilityModel ("ns3::RandomWalk2dMobilityModel",
                                 "Bounds", RectangleValue (Rectangle (-50, 50, -50, 50)));
      mobility.Install (wifiStaNodes);

      mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
      mobility.Install (wifiApNode);

    //Se instala la pila de Internet en cada una de las topologías
      InternetStackHelper stack;
      stack.Install (csmaNodes);
      stack.Install (wifiApNode);
      stack.Install (wifiStaNodes);

      Ipv4AddressHelper address;

    //Direccionamiento de cada una de las topologías.
      address.SetBase ("10.1.1.0", "255.255.255.0");
      Ipv4InterfaceContainer p2pInterfaces;
      p2pInterfaces = address.Assign (p2pDevices);

      address.SetBase ("10.1.2.0", "255.255.255.0");
      Ipv4InterfaceContainer csmaInterfaces;
      csmaInterfaces = address.Assign (csmaDevices);

      address.SetBase ("10.1.3.0", "255.255.255.0");
      address.Assign (staDevices);
      address.Assign (apDevices);


    //Configuración de la aplicación a modo del paradigma Cliente-Servidor
      UdpEchoServerHelper echoServer (9);

      ApplicationContainer serverApps = echoServer.Install (csmaNodes.Get (nCsma));
      serverApps.Start (Seconds (1.0));
      serverApps.Stop (Seconds (10.0));

      UdpEchoClientHelper echoClient (csmaInterfaces.GetAddress (nCsma), 9);
      echoClient.SetAttribute ("MaxPackets", UintegerValue (1));
      echoClient.SetAttribute ("Interval", TimeValue (Seconds (1.0)));
      echoClient.SetAttribute ("PacketSize", UintegerValue (1024));

      ApplicationContainer clientApps = 
        echoClient.Install (wifiStaNodes.Get (nWifi - 1));
      clientApps.Start (Seconds (2.0));
      clientApps.Stop (Seconds (10.0));

    //Tablas de enrutamiento genéricas
      Ipv4GlobalRoutingHelper::PopulateRoutingTables ();
      

      Simulator::Stop (Seconds (10.0));

      if (tracing == true)
        {
          pointToPoint.EnablePcapAll ("third");
          phy.EnablePcap ("third", apDevices.Get (0));
          csma.EnablePcap ("third", csmaDevices.Get (0), true);
        }


  AnimationInterface anim ("ejemplo1.xml");

    //Arranca la simulación en 2s y se elimina a los 10 s
      Simulator::Run ();
      Simulator::Destroy ();
      return 0;
    }
