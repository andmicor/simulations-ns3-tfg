#ifndef FUNCTIONS_H
#define FUNCTIONS_H

using namespace ns3;
using namespace std;

 int nWifi = 4; /// Number of STA WiFi nodes in our topology. Total number of nodes is (nWifi x 2) + node AP.
 
 set <Ipv4Address> des_ip; //Set of unique destination addresses

 vector<uint8_t> v_payload; //Serialized IPs to add in multicast's payload.


//Table of linked End-Devices Nodes
 struct table
 {
  Ptr<Node> myNode;
  uint32_t myID;
  uint32_t remoteID;
  Ipv4Address myIP;
  Ipv4Address remoteIP;
 };


 struct stats_time
 {
    uint64_t pkt_id;
    uint64_t multicast_id;
    uint64_t unicast_id;
    int64_t send_time;
    int64_t multi_time;
    int64_t recv_time;
    int64_t e2e_delay;
    Ipv4Address destination;
 };

 struct pkt_capture
 {
 	Ptr<Packet> pkt;
 	uint64_t pkt_id;
 	Ipv4Address destination;
 	int64_t timestamp;
 };

class MatchesID
{
    uint64_t _ID;

public:
    MatchesID(const uint64_t ID) : _ID(ID) {}

    bool operator()(const struct stats_time *item) const
    {
        return item->pkt_id == _ID;
    }
};

class MatchesID_multicast
{
    uint64_t _ID;

public:
    MatchesID_multicast(const uint64_t ID) : _ID(ID) {}

    bool operator()(const struct stats_time *item) const
    {
        return item->multicast_id == _ID;
    }
};

class MatchesID_unicast
{
    uint64_t _ID;

public:
    MatchesID_unicast(const uint64_t ID) : _ID(ID) {}

    bool operator()(const struct stats_time *item) const
    {
        return item->unicast_id == _ID;
    }
};





//Vector containing conections between End-Devices nodes
  vector<table> v_table;

// vector containing times and relationships between packets.
  vector<stats_time *> v_time;

  vector <pkt_capture> packets_vector;

  struct stats_time *t;


  int cont1=0;
  int cont2=0;
  int cont3=0;
  int cont_global=0;
  int cont_send=0;
  int cont_mitm=0;
  int cont_socketMITM_end =0;




//Function to plot a graph given a dataset.
void plotGraph(string filename, string x, string y, Gnuplot2dDataset dataset)
{

  string graphicsFileName        = filename + ".png";
  string plotFileName            = filename + ".plt";
  string plotTitle               = filename;
  string dataTitle               = filename;

  // Instantiate the plot and set its title.
  Gnuplot gnuplot (graphicsFileName);
  gnuplot.SetTitle (plotTitle);

  // Make the graphics file, which the plot file will be when it
  // is used with Gnuplot, be a PNG file.
  gnuplot.SetTerminal ("png");

  // Set the labels for each axis.
  gnuplot.SetLegend (x, y);

       
  dataset.SetTitle(dataTitle);
  dataset.SetStyle(Gnuplot2dDataset::HISTEPS);


  gnuplot.AddDataset (dataset);

  // Open the plot file.
  ofstream plotFile (plotFileName.c_str());

  // Write the plot file.
  gnuplot.GenerateOutput (plotFile);

  // Close the plot file.
  plotFile.close ();

}



void MITM_CR() // This cyclid method is called at main(). MITM_CR() extracts Destination IPs and send it through the payload.
//  ||Smart module||
{
  Simulator::Schedule(Seconds(0.1), &MITM_CR); //Cyclic.

   //Enable printing packet metadata
  Ipv4Address src_ip;
   
 // Use indicator to search the packet
  for(int i=0; i<(int) packets_vector.size(); i++)
  {
  PacketMetadata::ItemIterator metadataIterator = packets_vector[i].pkt->BeginItem();
  PacketMetadata::Item item;


  while (metadataIterator.HasNext())
  {

    item = metadataIterator.Next();
  //  NS_LOG_FUNCTION("item name: " << item.tid.GetName());
    
    // If we want to have an ip header
    if(item.tid.GetName() == "ns3::Ipv4Header")
    {

      Callback<ObjectBase *> constr = item.tid.GetConstructor();
      NS_ASSERT(!constr.IsNull());
 
      // Ptr<> and DynamicCast<> won't work here as all headers are from ObjectBase, not Object
      ObjectBase *instance = constr();
      NS_ASSERT(instance != 0);
 
      Ipv4Header* ipv4Header = dynamic_cast<Ipv4Header*> (instance);
      NS_ASSERT(ipv4Header != 0);
 
      ipv4Header->Deserialize(item.current);
 
      // The ipv4Header can now obtain the source of the packet
      src_ip = ipv4Header->GetSource();
      if (des_ip.find(ipv4Header->GetDestination()) == des_ip.end())
        des_ip.insert(ipv4Header->GetDestination());


      delete ipv4Header;
      break;
       
    }

  }
  }

   
//Serialize IPv4 destination addresses to Payload
  v_payload.clear();

  if(des_ip.size()!=0)
  {
    for (set<Ipv4Address>::iterator it=des_ip.begin(); it!=des_ip.end(); ++it)
  {
    uint8_t buf[4];
    it->Serialize(buf);
    v_payload.push_back(buf[0]);
    v_payload.push_back(buf[1]);
    v_payload.push_back(buf[2]);
    v_payload.push_back(buf[3]);
  }
  }
  

  uint8_t copia[v_payload.size()];
  copy(v_payload.begin(), v_payload.end(), copia);
  for(int i=0; i<(int)v_payload.size();i+=4)
  {
    cout<<"Contenido del vector al enviar el multicast: " << Ipv4Address::Deserialize(&copia[i]) <<endl;
  }
  

   //Multicast packet is created.
   Ptr<Packet> pkt_multicast;

   pkt_multicast= Create<Packet>((uint8_t *) copia,1024); //Create packet
 
  //Multicast packet containing Destination IPs of single LAN nodes
  TypeId tid = TypeId::LookupByName ("ns3::UdpSocketFactory");
  Ptr<Socket> socket=Socket::CreateSocket(NodeList::GetNode(0), tid); //A socket is created at RC's MiTM
  socket->Connect(InetSocketAddress("239.192.255.1",10));
  socket->SendTo(pkt_multicast, 0, InetSocketAddress("239.192.255.1",10)); //Multicast packet is sent
  socket->Close();

  for(int i=0; i<(int) packets_vector.size(); i++)
  {
  	
    vector<stats_time*>::iterator it = find_if( v_time.begin(), v_time.end(), MatchesID(packets_vector[i].pkt_id));
  //  (*it)->pkt_id=packets_vector[i].pkt_id;
    (*it)->destination = packets_vector[i].destination;
    (*it)->send_time= packets_vector[i].timestamp;
    (*it)->multicast_id = pkt_multicast->GetUid();
    (*it)->multi_time = Simulator::Now().GetMicroSeconds();
  }

  cout<<"Siguiente timeout de 100 ms. ++++Se envía paquete Multicast ... Time: "<< Simulator::Now().GetMilliSeconds() << endl;

  
//Clear all buffers
  packets_vector.clear();
  des_ip.clear();

 
  v_payload.clear();
  
}


//Promisc function on MiTM's RC. This method tries to store packets on a list for a future smart management.
bool capture_packets(Ptr<NetDevice> dev, Ptr<const Packet> packet, uint16_t proto, const Address &sender,const Address &receiver,
 ns3::NetDevice::PacketType packetType)
{

  cout<<"Callback promisc at: "<<Simulator::Now().GetMilliSeconds()<<endl;

  // To get a header from Ptr<Packet> packet first, copy the packet 
  Ptr<Packet> q = packet->Copy();

  Ipv4Header h;
  q->PeekHeader(h);

  struct pkt_capture p;
  p.pkt=q;
  p.pkt_id=q->GetUid();
  p.destination=h.GetDestination();
  p.timestamp=Simulator::Now().GetMicroSeconds();

  //Push packets to a vector
  packets_vector.push_back(p); 

  des_ip.clear();

  return true;
}


// MITM associated to the End Devices. ||Smart module||
void RecvMITM_DF (Ptr<Socket> socket)
{

  //Recv at End Device's MitM
  Ptr<Packet> packet=socket->Recv();

  cout <<"Se recibe un paquete en el MiTM: "<<socket->GetNode()->GetId()<< " Time: "<< Simulator::Now().GetMilliSeconds()<<endl;
  cont_mitm++;


//Vector containing IPv4 address of packets
  vector<Ipv4Address> direccion;
 
//Extract payload of multicast packet 
  uint8_t *buffer = new uint8_t[packet->GetSize ()];
  packet->CopyData(buffer, packet->GetSize());

//Deserialize payload
 for(int i=0; i<(4*nWifi); i+=4)
 {
  direccion.push_back(Ipv4Address::Deserialize(&buffer[i]));
 }


 for(int i=0; i<nWifi-1; i+=1)
 {
  cout<<"MiTM's addresses to send via unicast to End Devices: "<< direccion[i]<<endl;
 }
  
uint64_t id_pkt_df;
//Send unicast packets to End Devices.
for(int i=0; i<(int) v_table.size(); i++)
{
   
  if(socket->GetNode()->GetId()==v_table[i].myID)
  {
    for(int j=0; j<(int) direccion.size(); j++)
    {
      if(direccion[j]==v_table[i].remoteIP)
      {

        cout<<"+++++I am MiTM: " << v_table[i].myID << " and I send to: "<< direccion[j] << endl;
      
         Ptr<Packet> p= Create<Packet>((uint8_t *) "payload",1024);
       
        //Unicast packet containing Destination IPs of single LAN nodes
        socket->SendTo(p, 0, InetSocketAddress(v_table[i].remoteIP,9)); //unicast packet is sent
      	id_pkt_df=p->GetUid();
      

      	for(int i=nWifi-2; i>0;i--)
      	{
    		vector<stats_time*>::iterator it_aux;
    		vector<stats_time*>::iterator it = find_if(v_time.begin(), v_time.end(), MatchesID_multicast(packet->GetUid()));
    		(*it)->unicast_id=id_pkt_df-(nWifi-2);
    		it_aux=next(it,i);
    		(*it_aux)->unicast_id=(id_pkt_df+i)-(nWifi-2);
      	}
		
        
        cout<<"------->Sending to End Devices... Node Dest: "<< v_table[i].remoteID<< endl;
        cont_socketMITM_end++;
        direccion.clear();
          
      }

  }
  }
 
}

	
 	 
 	 
 
 


//Clear buffer
  memset(buffer, 0, packet->GetSize());

}



// Receive method in each End Device
void Recv_DF (Ptr<Socket> socket)
{
    Ptr<Packet> packet; 
    uint64_t id_unicast;
    packet = socket->Recv();
    id_unicast=packet->GetUid();
    cout<<"----Recv time: " <<Simulator::Now().GetMilliSeconds()<<endl;
    
    vector<stats_time*>::iterator it = find_if( v_time.begin(), v_time.end(), MatchesID_unicast(id_unicast));
    (*it)->recv_time=Simulator::Now().GetMicroSeconds();
    (*it)->e2e_delay=((*it)->recv_time)-((*it)->send_time);

      cout <<"Se recibe un paquete en el End Device: "<<socket->GetNode()->GetId()<<endl;
      


      cont_global++;

  
    if((socket->GetNode()->GetId())==6)
    {
      cont1++;
    }
    if(socket->GetNode()->GetId()==7)
    {
      cont2++;
    }
    if(socket->GetNode()->GetId()==8)
    {
      cont3++;
    }

}


//Generate traffic on RC
static void GenerateTraffic (Ptr<Socket> socket, uint32_t pktSize, 
                             uint32_t pktCount, Time pktInterval )
{
   
  if (pktCount > 0)
    {
      cout<<"++++Send time: " << Simulator::Now().GetMilliSeconds()<<endl;
     
      t=new struct stats_time;
     
      Ptr<Packet> packet= Create<Packet> (pktSize);
      t->pkt_id=packet->GetUid();

      socket->Send(packet);
      cont_send++;

      Simulator::Schedule (pktInterval, &GenerateTraffic, 
                           socket, pktSize,pktCount-1, pktInterval);
      v_time.push_back(t);
      
    }

  else
    {
      socket->Close ();
    }
}



#endif