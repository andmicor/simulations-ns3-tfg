/*! \file simulacion2.cc
  \brief Second Simulation. We got a hybrid LAN/P2P link topology, trying to configure a MiTM in hybrid nodes and solve the congestion generated in the First Simulation. NS3 libraries are needed to simulate the topology, including wifi-module, the 
  NS3 GUI Netanim and the Flow Monitor. We need to represent a Multicast frame topology and calculating congestion using report files generated.
*/

#include <iostream>
#include <cstring>
#include <vector>
#include <algorithm>
#include <fstream>


#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/applications-module.h"
#include "ns3/wifi-module.h"
#include "ns3/mobility-module.h"
#include "ns3/csma-module.h"
#include "ns3/internet-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/netanim-module.h"
#include "ns3/flow-monitor-module.h"
#include "ns3/node-container.h"
#include "ns3/node.h"
#include "ns3/address.h"
#include "ns3/net-device.h"
#include "ns3/inet-socket-address.h"
#include "ns3/gnuplot.h"
#include "ns3/stats-module.h"

#include "functions.h"


/// Default Network Topology
//
///  * -> WiFi access
///                                          AP
///  *         *         *                   * 
///  |    |    |         |    
/// n0----n5   n1----n6   n2----n7  .....   n4
///   p2p        p2p          p2p
//                               


  NS_LOG_COMPONENT_DEFINE ("Simulacion2");




int 
main (int argc, char *argv[])
{

 
  bool verbose = true;/// Program log show in the output 
  bool tracing = false; /// For Wireshark representation
  //double rss=-30; /// Received signal in dB from AP


  uint32_t packetSize = 1024;
  uint32_t packetCount = 100;
  double packetInterval = 0.1;

  Time start_send=Seconds(2.005);
  Time stop_simulation=Seconds(10.0);
  Time start_MITM=Seconds(2.1);

  CommandLine cmd;
  cmd.AddValue ("nWifi", "Number of wifi STA devices", nWifi);
  cmd.AddValue ("verbose", "Tell echo applications to log if true", verbose);
  cmd.AddValue ("tracing", "Enable pcap tracing", tracing);

  cmd.Parse (argc,argv);


  if (verbose)
    {
     LogComponentEnable ("UdpEchoClientApplication", LOG_LEVEL_INFO);
     LogComponentEnable ("UdpEchoServerApplication", LOG_LEVEL_INFO);
    }

///Setting Wireless nodes 
  NodeContainer wifiStaNodes; /// Statics 
  wifiStaNodes.Create (nWifi); 
  NodeContainer wifiApNode;
  wifiApNode.Create(1);/// To route all packets, an AP node is generated  

   

/// Medium is configured. IEEE 802.11. 
  YansWifiChannelHelper channel = YansWifiChannelHelper::Default (); /// We set the default channel. 
  YansWifiPhyHelper phy = YansWifiPhyHelper::Default ();
  phy.SetChannel (channel.Create ()); /// Channel is created. 



  double maxPower = 30;
  phy.Set("TxPowerStart", DoubleValue(maxPower));
  phy.Set("TxPowerEnd", DoubleValue(maxPower));

/* \brief This is one parameter that matters when using FixedRssLossModel
   set it to zero; otherwise, gain will be added 
  phy.Set ("RxGain", DoubleValue (0));

  /// ns-3 supports RadioTap and Prism tracing extensions for 802.11g
  phy.SetPcapDataLinkType (YansWifiPhyHelper::DLT_IEEE802_11_RADIO);

/// ns-3 supports RadioTap and Prism tracing extensions for 802.11g
  channel.SetPropagationDelay ("ns3::ConstantSpeedPropagationDelayModel");

   \brief The below FixedRssLossModel will cause the rss to be fixed regardless
   of the distance between the two stations, and the transmit power
  channel.AddPropagationLoss ("ns3::FixedRssLossModel","Rss",
    DoubleValue (rss));
  phy.SetChannel (channel.Create ());
*/

 /// Configuring WiFi
  WifiHelper wifi;


  wifi.SetRemoteStationManager ("ns3::AarfWifiManager");


/// Setting both Level two layer in AP and static nodes
  WifiMacHelper mac;
  Ssid ssid = Ssid ("ns-3-ssid");
  mac.SetType ("ns3::StaWifiMac",
               "Ssid", SsidValue (ssid));

/// Installing static devices
  NetDeviceContainer staDevices;
  staDevices = wifi.Install (phy, mac, wifiStaNodes);

 /// AP device 
  mac.SetType ("ns3::ApWifiMac",
               "Ssid", SsidValue (ssid));


/// Installing AP devices
  NetDeviceContainer apDevices;
  apDevices = wifi.Install (phy, mac, wifiApNode);


/// Setting a mobility model in static nodes
  MobilityHelper mobility;

  mobility.SetPositionAllocator ("ns3::GridPositionAllocator",
                                 "MinX", DoubleValue (0.0),
                                 "MinY", DoubleValue (0.0),
                                 "DeltaX", DoubleValue (50.0),
                                 "DeltaY", DoubleValue (10.0),
                                 "GridWidth", UintegerValue (3),
                                 "LayoutType", StringValue ("RowFirst"));
/// Setting a mobility model in static nodes
  mobility.SetMobilityModel ("ns3::RandomWalk2dMobilityModel",
                             "Bounds", RectangleValue (Rectangle (-1000, 1000, -1000, 1000)));
  mobility.Install (wifiStaNodes);

/// AP is set as immovable
  mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
  mobility.Install (wifiApNode);


//To configure hybrid LAN/P2P nodes
  NodeContainer *lan[nWifi];
  NodeContainer newLanNodes[nWifi];
  Ipv4InterfaceContainer p2p_address;
  Ipv4InterfaceContainer mitm_lan_address;
           



/// Internet stack is needed to be installed in both kind of nodes
  InternetStackHelper internet_olsr;
  internet_olsr.Install (wifiStaNodes);
  internet_olsr.Install (wifiApNode);
  InternetStackHelper internet_p2p;



  Ipv4AddressHelper address;
/// Setting Adressing on Wifi nodes
  address.SetBase ("192.168.1.0", "255.255.255.0");
  Ipv4InterfaceContainer sta_address = address.Assign (staDevices);
  Ipv4InterfaceContainer ap_address =  address.Assign (apDevices);

   
 
  NetDeviceContainer p2pDevices[nWifi];
//Configuring hybrid nodes and assignning IPs
  for(int i=0; i<nWifi; i++)
  { 


  char buffer[50];
  sprintf(buffer,"192.168.%d.0", i+2);
  address.SetBase (buffer, "255.255.255.0");
   
  newLanNodes[i].Create (1);
  internet_p2p.Install (newLanNodes[i]);

   
  lan[i]= new NodeContainer(wifiStaNodes.Get(i), newLanNodes[i]);
   
  PointToPointHelper p2p;
  p2p.SetDeviceAttribute ("DataRate", StringValue ("5Mbps"));
  p2p.SetChannelAttribute ("Delay", StringValue ("13us"));

  p2pDevices[i] = p2p.Install(*lan[i]) ;

    
  
  mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
  mobility.Install (*lan[i]);
   
 
  Ipv4InterfaceContainer singleP2P_address = address.Assign (p2pDevices[i]);


  p2p_address.Add(singleP2P_address.Get(1));
  mitm_lan_address.Add(singleP2P_address.Get(0));
  
  }




NS_LOG_INFO ("Configure multicasting.");

  Ipv4Address multicastSource (sta_address.GetAddress(0));
  Ipv4Address multicastGroup ("239.192.255.1");


  Ipv4StaticRoutingHelper multicast;
  Ptr<Node> multicastRouter = wifiStaNodes.Get (0);  // The node in question
  Ptr<NetDevice> inputIf = staDevices.Get (0);  // The input NetDevice
  NetDeviceContainer outputDevices;  // A container of output NetDevices
  outputDevices.Add (staDevices.Get (0));  // (we only need one NetDevice here)

  multicast.AddMulticastRoute (multicastRouter, multicastSource, 
                               multicastGroup, inputIf, outputDevices);


    
    //sender of the multicast datagram
  for(int i=0; i<nWifi; i++)
  {

   Ptr<Node> sender = wifiStaNodes.Get (0);
   Ptr<NetDevice> senderIf = staDevices.Get (0);

   multicast.SetDefaultMulticastRoute (sender, senderIf);

  
  }


  NS_LOG_INFO ("Create sockets.");
  //Receiver socket on n1
  TypeId tid = TypeId::LookupByName ("ns3::UdpSocketFactory");

  //Sender socket on n0
  Ptr<Socket> source[nWifi];
  for(int i=0; i<nWifi; i++)
    source[i] =  Socket::CreateSocket (newLanNodes[0].Get (0), tid);


//  Receiver sockets
  Ptr<Socket> recvSink[nWifi];

  InetSocketAddress local = InetSocketAddress (Ipv4Address::GetAny(), 10);

  for(int i=1;i<nWifi; i++)
  {
    recvSink[i] =  Socket::CreateSocket (wifiStaNodes.Get (i), tid);
    recvSink[i]->Bind (local);
    recvSink[i]->SetRecvCallback (MakeCallback (&RecvMITM_DF));
  }
 
InetSocketAddress local2 = InetSocketAddress (Ipv4Address::GetAny(), 9);
   Ptr<Socket> recvSinkNewLans[nWifi];
     for (int i=1; i<nWifi; i++)
  {
    recvSinkNewLans[i] =  Socket::CreateSocket (newLanNodes[i].Get (0), tid);
    recvSinkNewLans[i]->Bind (local2);
    recvSinkNewLans[i]->SetRecvCallback (MakeCallback (&Recv_DF));
  }

  
  
  p2pDevices[0].Get(0)->SetPromiscReceiveCallback(MakeCallback(&capture_packets)); 
  Simulator::Schedule(start_MITM, &MITM_CR);

  Time interPacketInterval = Seconds (packetInterval);
  // Transmitter socket connections. Set transmitter for broadcasting
  InetSocketAddress *remote[nWifi];
   for(int i=1; i<nWifi; i++)
   {
     remote[i]= new InetSocketAddress(p2p_address.GetAddress(i), 10);
     source[i]->SetAllowBroadcast (true); 
     source[i]->Connect (*remote[i]);

     Simulator::ScheduleWithContext (source[i]->GetNode ()->GetId (),
                                  start_send, &GenerateTraffic, 
                                  source[i], packetSize, packetCount, interPacketInterval);
   }


  
Packet::EnablePrinting();  


//----------------------------------------------------------------------
/// Generic Routing Tables.

 Ipv4GlobalRoutingHelper::PopulateRoutingTables ();
 

//Creating End Devices relationship table
 for(int i=1; i<nWifi; i++)
 {

  struct table t;
  t.myNode=wifiStaNodes.Get(i);
  t.myID=wifiStaNodes.Get(i)->GetId();
  t.remoteID=newLanNodes[i].Get(0)->GetId();
  t.myIP=mitm_lan_address.GetAddress(i);
  t.remoteIP=p2p_address.GetAddress(i);
  v_table.push_back(t);
 }


  

   // Trace routing tables
   Ipv4GlobalRoutingHelper g;
   Ptr<OutputStreamWrapper> routingStream = Create<OutputStreamWrapper>
("RoutingTable", std::ios::out);
   g.PrintRoutingTableAllAt (Seconds (12), routingStream);

/// Flow Monitor processing.
   Ptr<FlowMonitor> flowMonitor;
   FlowMonitorHelper flowHelper;
   flowMonitor = flowHelper.InstallAll();

  
  
  
 
   Simulator::Stop (stop_simulation);


/// Wireshark trace file.
  if (tracing == true)
    {
      phy.EnablePcap ("simulacion2_pcap", staDevices);

    }

/// Enable animation with Netanim.
  AnimationInterface anim ("animacion2.xml");


/// Enable metadata, very useful to represent in Netanim.
  anim.EnablePacketMetadata (true);

  Simulator::Run ();
  Packet::EnablePrinting();


 

  Gnuplot2dDataset dataset;
    
  //Flow Monitor ... continued

 
    double Throughput=0.0;

    flowMonitor->CheckForLostPackets ();
    Ptr<Ipv4FlowClassifier> classifier = DynamicCast<Ipv4FlowClassifier> (flowHelper.GetClassifier ());
    std::map<FlowId, FlowMonitor::FlowStats> stats = flowMonitor->GetFlowStats ();

    for (std::map<FlowId, FlowMonitor::FlowStats>::const_iterator iter = stats.begin (); iter != stats.end (); ++iter)
      {
     // Ipv4FlowClassifier::FiveTuple t = classifier->FindFlow (iter->first);

   /*     NS_LOG_UNCOND("Flow ID: " << iter->first << " Src Addr " << t.sourceAddress << " Dst Addr " << t.destinationAddress);
        NS_LOG_UNCOND("Tx Packets = " << iter->second.txPackets);
        NS_LOG_UNCOND("Rx Packets = " << iter->second.rxPackets);
        NS_LOG_UNCOND("Delay Sum = " << iter->second.delaySum);
        NS_LOG_UNCOND("Delay = " << iter->second.delaySum / iter->second.rxPackets << "ns");
        NS_LOG_UNCOND("Delay = " << (iter->second.delaySum / iter->second.rxPackets)/1000000 << "ms");
        NS_LOG_UNCOND("Jitter Sum = " << iter->second.jitterSum);
        NS_LOG_UNCOND("Jitter = " << iter->second.jitterSum / (iter->second.rxPackets - 1) << "ns");
        //NS_LOG_UNCOND("Jitter = " << (iter->second.jitterSum / (iter->second.rxPackets - 1))/1000000 << "ms");
        NS_LOG_UNCOND("Throughput: " << iter->second.rxBytes * 8.0 / (iter->second.timeLastRxPacket.GetSeconds()-iter->second.timeFirstTxPacket.GetSeconds()) / 1024  << " Kbps");*/
        Throughput=iter->second.rxBytes * 8.0 /
                  (iter->second.timeLastRxPacket.GetSeconds()-iter->second.timeFirstTxPacket.GetSeconds())
                  / 1024;

   
        dataset.Add((double)iter->first,(double) Throughput);
            
       /* NS_LOG_UNCOND("Lost Packets = " << iter->second.lostPackets);
        NS_LOG_UNCOND("-------------------------------------------------");
        NS_LOG_UNCOND("-------------------------------------------------");*/

      }


  //Gnuplot
  plotGraph("Flow vs Throughput", "Flow", "Throughput",dataset);

    

  flowMonitor->SerializeToXmlFile("monitor.flowmonitor2", true, true);
 



  Simulator::Destroy ();

  ofstream csvfile;
  csvfile.open ("stats_simulation2.csv");

  for(int i=0; i<(int) v_time.size(); i++)
  {
  	csvfile<<v_time[i]->destination<<","<<v_time[i]->pkt_id << "," <<v_time[i]->multicast_id << ","<< v_time[i]->unicast_id << "," <<
    v_time[i]->send_time << "," <<v_time[i]->multi_time <<"," <<v_time[i]->recv_time << 
    "," <<v_time[i]->e2e_delay<<endl ;
  }
  csvfile.close();

  cout<<endl<<endl;
  cout<<"       ----Stats----      "<<endl;
  cout<<"Recv Paquetes nodo 6: "<<cont1<<endl;
  cout<<"Recv Paquetes nodo 7: "<<cont2<<endl;
  cout<<"Recv Paquetes nodo 8: "<<cont3<<endl;
  cout<<"Sent Packet MITM End Nodes: "<< cont_socketMITM_end << endl;
  cout<< "Total Recv packets at MiTM: " << cont_mitm << endl;
  cout<< "Total Recv packets at End Devices: " << cont_global << endl;
  cout<< "Total Send packets at RC: " << cont_send << endl;

  cout<<endl<<endl;

  cout<<"----MITM relationship table----"<<endl;
  for(int i=0;i<(int)v_table.size();i++)
  {
    cout<<"MyID: " <<v_table[i].myID << " | " << "RemoteID: " <<v_table[i].remoteID << " | " <<"MyIP: " <<v_table[i].myIP << " | " <<"Remote IP: " <<v_table[i].remoteIP << endl;
  }
  cout<<endl<<endl;
  cout<<"----Time Stats----"<<endl;

  for(int i=0; i<(int) v_time.size(); i++)
  {
    cout<<"Destination IP: " << v_time[i]->destination<<" | "<<"Pkt ID: " <<v_time[i]->pkt_id << " | " << "Multicast ID: " <<v_time[i]->multicast_id << " | "<< "Unicast ID: " <<v_time[i]->unicast_id << " | " <<"Send time: " <<
    v_time[i]->send_time << " | " <<"Multi Time: " <<v_time[i]->multi_time <<" | " <<"Recv Time: " <<v_time[i]->recv_time << 
    " | " <<"E2E Delay: " <<v_time[i]->e2e_delay <<endl ;
  }


  for(int i=0; i<(int) packets_vector.size(); i++)
  {
  //	cout<<"ID Pkt: " <<packets_vector[i].pkt_id << " | " <<"Destination: " <<packets_vector[i].destination << " | " <<"Send Time: " << 
  //	packets_vector[i].timestamp<<endl;
  }

  return 0;
}
