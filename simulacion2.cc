/*! \file simulacion2.cc
  \brief Second Simulation. We got a hybrid LAN/P2P link topology, trying to configure a MiTM in hybrid nodes and solve the congestion generated in the First Simulation. NS3 libraries are needed to simulate the topology, including wifi-module, the 
  NS3 GUI Netanim and the Flow Monitor. We need to represent a Multicast frame topology and calculating congestion using report files generated.
*/

#include <iostream>
#include <cstring>
#include <vector>

#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/applications-module.h"
#include "ns3/wifi-module.h"
#include "ns3/mobility-module.h"
#include "ns3/csma-module.h"
#include "ns3/internet-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/netanim-module.h"
#include "ns3/flow-monitor-module.h"
#include "ns3/node-container.h"
#include "ns3/node.h"
#include "ns3/address.h"
#include "ns3/net-device.h"
#include "ns3/inet-socket-address.h"
#include "ns3/gnuplot.h"
#include "ns3/stats-module.h"
#include "ns3/tag.h"


/// Default Network Topology
//
///  * -> WiFi access
///                                          AP
///  *         *        *                    * 
///  |    |    |         |    
/// n0----n5   n1----n6   n2----n7  .....   n4
///   p2p        p2p          p2p
//                               



using namespace ns3;
using namespace std;


 int nWifi = 4; /// Number of STA WiFi nodes in our topology. Total number of nodes is (nWifi x 2) + node AP.
 vector <Ptr<Packet>> dir;
 set <Ipv4Address> des_ip; //Set of unique destination addresses

 vector<uint8_t> v_payload;

 struct table
 {
  Ptr<Node> myNode;
  uint32_t myID;
  uint32_t remoteID;
  Ipv4Address myIP;
  Ipv4Address remoteIP;
 };

  std::vector<table> v_table;


  int cont1=0;
  int cont2=0;
  int cont3=0;
  int cont_global=0;
  int cont_send=0;
  int cont_mitm=0;
  int cont_socketMITM_end =0;
 
  double tiempo1=0.0;
  double tiempo2=0.0;


  uint32_t bytesTotal=0;
  Ptr<TimeMinMaxAvgTotalCalculator> delay;
  double rcv=0.0;
  double sqhd=0.0;



NS_LOG_COMPONENT_DEFINE ("Simulacion2");



void plotGraph(string filename, string x, string y, Gnuplot2dDataset dataset)
{

  string graphicsFileName        = filename + ".png";
  string plotFileName            = filename + ".plt";
  string plotTitle               = filename;
  string dataTitle               = filename;

  // Instantiate the plot and set its title.
  Gnuplot gnuplot (graphicsFileName);
  gnuplot.SetTitle (plotTitle);

  // Make the graphics file, which the plot file will be when it
  // is used with Gnuplot, be a PNG file.
  gnuplot.SetTerminal ("png");

  // Set the labels for each axis.
  gnuplot.SetLegend (x, y);

       
  dataset.SetTitle(dataTitle);
  dataset.SetStyle(Gnuplot2dDataset::HISTEPS);


  gnuplot.AddDataset (dataset);

  // Open the plot file.
  ofstream plotFile (plotFileName.c_str());

  // Write the plot file.
  gnuplot.GenerateOutput (plotFile);

  // Close the plot file.
  plotFile.close ();

}



void manipulate_packet() // This cyclid method is called at main(). manipulate_packet() extracts Destination IPs and send it through the payload.
//  ||Smart module||
{
  Simulator::Schedule(Seconds(0.1), &manipulate_packet); //Cyclic.
   //Enable printing packet metadata
  Ipv4Address src_ip;

       
 // Use indicator to search the packet
  for(int i=0; i<(int) dir.size(); i++)
  {
  PacketMetadata::ItemIterator metadataIterator = dir[i]->BeginItem();
  PacketMetadata::Item item;


  while (metadataIterator.HasNext())
  {

    item = metadataIterator.Next();
    NS_LOG_FUNCTION("item name: " << item.tid.GetName());
    
    // If we want to have an ip header
    if(item.tid.GetName() == "ns3::Ipv4Header")
    {

      Callback<ObjectBase *> constr = item.tid.GetConstructor();
      NS_ASSERT(!constr.IsNull());
 
      // Ptr<> and DynamicCast<> won't work here as all headers are from ObjectBase, not Object
      ObjectBase *instance = constr();
      NS_ASSERT(instance != 0);
 
      Ipv4Header* ipv4Header = dynamic_cast<Ipv4Header*> (instance);
      NS_ASSERT(ipv4Header != 0);
 
      ipv4Header->Deserialize(item.current);
 
      // The ipv4Header can now obtain the source of the packet
      src_ip = ipv4Header->GetSource();
      des_ip.insert(ipv4Header->GetDestination());

      
      
      // Finished, clear the ip header and go back
      delete ipv4Header;
      break;

    }

  }
  
  }

  Ptr<Packet> pkt_multicast;
  
  dir.clear(); 

  //Create packets with destination address and source. Custom headers.

  v_payload.clear();

  if(des_ip.size()!=0)
  {
    for (set<Ipv4Address>::iterator it=des_ip.begin(); it!=des_ip.end(); ++it)
  {
    uint8_t buf[4];
    it->Serialize(buf);
    v_payload.push_back(buf[0]);
    v_payload.push_back(buf[1]);
    v_payload.push_back(buf[2]);
    v_payload.push_back(buf[3]);
  }
  }
  

  des_ip.clear();
  uint8_t copia[v_payload.size()];
  copy(v_payload.begin(), v_payload.end(), copia);
  for(int i=0; i<(int)v_payload.size();i+=4)
  {
    cout<<"Contenido del vector al enviar el multicast: " << Ipv4Address::Deserialize(&copia[i])<<endl;
  }
  

    pkt_multicast= Create<Packet>((uint8_t *) copia,1024);


  //Multicast packet containing Destination IPs of single LAN nodes
  TypeId tid = TypeId::LookupByName ("ns3::UdpSocketFactory");
  Ptr<Socket> socket=Socket::CreateSocket(NodeList::GetNode(0), tid); //A socket is created at RC's MiTM
  socket->Connect(InetSocketAddress("239.192.255.1",10));
  socket->SendTo(pkt_multicast, 0, InetSocketAddress("239.192.255.1",10)); //Multicast packet is sent
  socket->Close();

  cout<<"Siguiente timeout de 100 ms. ++++Se envía paquete Multicast ... Time: "<< Simulator::Now().GetMilliSeconds() << endl;

  

  des_ip.clear();

 
  v_payload.clear();
  
 
}


bool callback_promisc(Ptr<NetDevice> dev, Ptr<const Packet> packet, uint16_t proto, const Address &sender,const Address &receiver,
 ns3::NetDevice::PacketType packetType)
{//Promisc function on MiTM's RC. This method tries to store packets on a list for a future smart management.

  cout<<"Callback promisc at: "<<Simulator::Now().GetMilliSeconds()<<endl;

  // To get a header from Ptr<Packet> packet first, copy the packet 
  Ptr<Packet> q = packet->Copy();

  dir.push_back(q);

  des_ip.clear();

	return true;
}


void ReceivePacket (Ptr<Socket> socket)
// MITM associated to the Robots. ||Smart module||
{


  Ptr<Packet> packet=socket->Recv();
  cout<<"ID del paquete: " <<packet->GetUid()<<endl;

  cout <<"Se recibe un paquete en el MiTM: "<<socket->GetNode()->GetId()<< " Time: "<< Simulator::Now().GetMilliSeconds()<<endl;
  cont_mitm++;

  vector<Ipv4Address> direccion;
 
  uint8_t *buffer = new uint8_t[packet->GetSize ()];
  packet->CopyData(buffer, packet->GetSize());


 

 for(int i=0; i<(4*nWifi); i+=4)
 {
  direccion.push_back(Ipv4Address::Deserialize(&buffer[i]));
 }


 for(int i=0; i<nWifi-1; i+=1)
 {
  cout<<"MiTM's addresses to send via unicast to End Devices: "<< direccion[i]<<endl;
 }
  

 cout<<"/////Tamaño del vector de direcciones en el MiTM/////: "<< direccion.size()<<endl;

for(int i=0; i<(int) v_table.size(); i++)
{
  if(socket->GetNode()->GetId()==v_table[i].myID)
  {
    for(int j=0; j<(int) direccion.size(); j++)
    {
      if(direccion[j]==v_table[i].remoteIP)
      {
         cout<<"+++++I am MiTM: " << v_table[i].myID << " and I send to: "<< direccion[j] << endl;
        Ptr<Packet> p= Create<Packet>((uint8_t *) "payload",1024);
  

        //Unicast packet containing Destination IPs of single LAN nodes
      //  Ptr<Socket> mysocket=Socket::CreateSocket(v_table[i].myNode, tid); //A socket is created at every MiTM
       // socket->Connect(InetSocketAddress(v_table[i].remoteIP,9));
        socket->SendTo(p, 0, InetSocketAddress(v_table[i].remoteIP,9)); //unicast packet is sent
       

        cout<<"------->Sending to End Devices... Node Dest: "<< v_table[i].remoteID<< endl;
        cont_socketMITM_end++;
        direccion.clear();
          


      }

  }
  }

}
  memset(buffer, 0, packet->GetSize());
  //socket->ShutdownRecv();
}




void ReceivePacketNewLans (Ptr<Socket> socket)
// Receive method in each Robot
{
    Ptr<Packet> packet; 
    packet = socket->Recv();
  
      cout <<"Se recibe un paquete en el End Device: "<<socket->GetNode()->GetId()<<endl;
      cont_global++;
      tiempo2=Simulator::Now().GetMilliSeconds();
      cout<<"----Time Recv: " <<tiempo2<<endl;
      //finish.push_back(tiempo2);

      bytesTotal += packet->GetSize ();
      //rcv = Simulator::Now().GetMilliSeconds();
      //sqhd = seqTsx.GetTs().GetMilliSeconds();

     // delay = delay +  (rcv - sqhd); //delay calculation
     // cout<<"----RCV-SQHD: "<< rcv-sqhd<<endl;

    
  

    if((socket->GetNode()->GetId())==6)
    {
      cont1++;
    }
       if(socket->GetNode()->GetId()==7)
    {
      cont2++;
    }
       if(socket->GetNode()->GetId()==8)
    {
      cont3++;
    }



}



static void GenerateTraffic (Ptr<Socket> socket, uint32_t pktSize, 
                             uint32_t pktCount, Time pktInterval )
{
   
  if (pktCount > 0)
    {
      cout<<"++++Send time: " << Simulator::Now().GetMilliSeconds()<<endl;
      tiempo1=Simulator::Now().GetMilliSeconds();
    //  start.push_back(tiempo1);
     
      Ptr<Packet> packet= Create<Packet> (pktSize);
      
      socket->Send(packet);
      cont_send++;

      Simulator::Schedule (pktInterval, &GenerateTraffic, 
                           socket, pktSize,pktCount-1, pktInterval);
      
    }

  else
    {
      socket->Close ();
    }
}

int 
main (int argc, char *argv[])
{

 
  bool verbose = true;/// Program log show in the output 
  bool tracing = false; /// For Wireshark representation
  //double rss=-30; /// Received signal in dB from AP


  uint32_t packetSize = 1024;
  uint32_t packetCount = 100;
  double packetInterval = 0.1;

  CommandLine cmd;
  cmd.AddValue ("nWifi", "Number of wifi STA devices", nWifi);
  cmd.AddValue ("verbose", "Tell echo applications to log if true", verbose);
  cmd.AddValue ("tracing", "Enable pcap tracing", tracing);

  cmd.Parse (argc,argv);


  if (verbose)
    {
     LogComponentEnable ("UdpEchoClientApplication", LOG_LEVEL_INFO);
     LogComponentEnable ("UdpEchoServerApplication", LOG_LEVEL_INFO);
    }

///Setting Wireless nodes 
  NodeContainer wifiStaNodes; /// Statics 
  wifiStaNodes.Create (nWifi); 
  NodeContainer wifiApNode;
  wifiApNode.Create(1);/// To route all packets, an AP node is generated  

   

/// Medium is configured. IEEE 802.11. 
  YansWifiChannelHelper channel = YansWifiChannelHelper::Default (); /// We set the default channel. 
  YansWifiPhyHelper phy = YansWifiPhyHelper::Default ();
  phy.SetChannel (channel.Create ()); /// Channel is created. 



  double maxPower = 30;
  phy.Set("TxPowerStart", DoubleValue(maxPower));
  phy.Set("TxPowerEnd", DoubleValue(maxPower));

/* \brief This is one parameter that matters when using FixedRssLossModel
   set it to zero; otherwise, gain will be added 
  phy.Set ("RxGain", DoubleValue (0));

  /// ns-3 supports RadioTap and Prism tracing extensions for 802.11g
  phy.SetPcapDataLinkType (YansWifiPhyHelper::DLT_IEEE802_11_RADIO);

/// ns-3 supports RadioTap and Prism tracing extensions for 802.11g
  channel.SetPropagationDelay ("ns3::ConstantSpeedPropagationDelayModel");

   \brief The below FixedRssLossModel will cause the rss to be fixed regardless
   of the distance between the two stations, and the transmit power
  channel.AddPropagationLoss ("ns3::FixedRssLossModel","Rss",
    DoubleValue (rss));
  phy.SetChannel (channel.Create ());
*/

 /// Configuring WiFi
  WifiHelper wifi;


  wifi.SetRemoteStationManager ("ns3::AarfWifiManager");


/// Setting both Level two layer in AP and static nodes
  WifiMacHelper mac;
  Ssid ssid = Ssid ("ns-3-ssid");
  mac.SetType ("ns3::StaWifiMac",
               "Ssid", SsidValue (ssid));

/// Installing static devices
  NetDeviceContainer staDevices;
  staDevices = wifi.Install (phy, mac, wifiStaNodes);

 /// AP device 
  mac.SetType ("ns3::ApWifiMac",
               "Ssid", SsidValue (ssid));


/// Installing AP devices
  NetDeviceContainer apDevices;
  apDevices = wifi.Install (phy, mac, wifiApNode);


/// Setting a mobility model in static nodes
  MobilityHelper mobility;

  mobility.SetPositionAllocator ("ns3::GridPositionAllocator",
                                 "MinX", DoubleValue (0.0),
                                 "MinY", DoubleValue (0.0),
                                 "DeltaX", DoubleValue (50.0),
                                 "DeltaY", DoubleValue (10.0),
                                 "GridWidth", UintegerValue (3),
                                 "LayoutType", StringValue ("RowFirst"));
/// Setting a mobility model in static nodes
  mobility.SetMobilityModel ("ns3::RandomWalk2dMobilityModel",
                             "Bounds", RectangleValue (Rectangle (-1000, 1000, -1000, 1000)));
  mobility.Install (wifiStaNodes);

/// AP is set as immovable
  mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
  mobility.Install (wifiApNode);


//To configure hybrid LAN/P2P nodes
  NodeContainer *lan[nWifi];
  NodeContainer newLanNodes[nWifi];
  Ipv4InterfaceContainer p2p_address;
  Ipv4InterfaceContainer mitm_lan_address;
           



/// Internet stack is needed to be installed in both kind of nodes
  InternetStackHelper internet_olsr;
  internet_olsr.Install (wifiStaNodes);
  internet_olsr.Install (wifiApNode);
  InternetStackHelper internet_p2p;



  Ipv4AddressHelper address;
/// Setting Adressing on Wifi nodes
  address.SetBase ("192.168.1.0", "255.255.255.0");
  Ipv4InterfaceContainer sta_address = address.Assign (staDevices);
  Ipv4InterfaceContainer ap_address =  address.Assign (apDevices);

   
 
  NetDeviceContainer p2pDevices[nWifi];
//Configuring hybrid nodes and assignning IPs
  for(int i=0; i<nWifi; i++)
  { 


  char buffer[50];
  sprintf(buffer,"192.168.%d.0", i+2);
  address.SetBase (buffer, "255.255.255.0");
   
  newLanNodes[i].Create (1);
  internet_p2p.Install (newLanNodes[i]);

   
  lan[i]= new NodeContainer(wifiStaNodes.Get(i), newLanNodes[i]);
   
  PointToPointHelper p2p;
  p2p.SetDeviceAttribute ("DataRate", StringValue ("5Mbps"));
  p2p.SetChannelAttribute ("Delay", StringValue ("13us"));

  p2pDevices[i] = p2p.Install(*lan[i]) ;

    
  
  mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
  mobility.Install (*lan[i]);
   
 
  Ipv4InterfaceContainer singleP2P_address = address.Assign (p2pDevices[i]);


  p2p_address.Add(singleP2P_address.Get(1));
  mitm_lan_address.Add(singleP2P_address.Get(0));
  
  }




NS_LOG_INFO ("Configure multicasting.");

  Ipv4Address multicastSource (sta_address.GetAddress(0));
  Ipv4Address multicastGroup ("239.192.255.1");


  Ipv4StaticRoutingHelper multicast;
  Ptr<Node> multicastRouter = wifiStaNodes.Get (0);  // The node in question
  Ptr<NetDevice> inputIf = staDevices.Get (0);  // The input NetDevice
  NetDeviceContainer outputDevices;  // A container of output NetDevices
  outputDevices.Add (staDevices.Get (0));  // (we only need one NetDevice here)

  multicast.AddMulticastRoute (multicastRouter, multicastSource, 
                               multicastGroup, inputIf, outputDevices);


    
    //sender of the multicast datagram
  for(int i=0; i<nWifi; i++)
  {

   Ptr<Node> sender = wifiStaNodes.Get (0);
   Ptr<NetDevice> senderIf = staDevices.Get (0);

   multicast.SetDefaultMulticastRoute (sender, senderIf);

  
  }


  NS_LOG_INFO ("Create sockets.");
  //Receiver socket on n1
  TypeId tid = TypeId::LookupByName ("ns3::UdpSocketFactory");

  //Sender socket on n0
  Ptr<Socket> source[nWifi];
  for(int i=0; i<nWifi; i++)
    source[i] =  Socket::CreateSocket (newLanNodes[0].Get (0), tid);


//  Receiver sockets
  Ptr<Socket> recvSink[nWifi];

  InetSocketAddress local = InetSocketAddress (Ipv4Address::GetAny(), 10);

  for(int i=1;i<nWifi; i++)
  {
    recvSink[i] =  Socket::CreateSocket (wifiStaNodes.Get (i), tid);
    recvSink[i]->Bind (local);
    recvSink[i]->SetRecvCallback (MakeCallback (&ReceivePacket));
  }
 
InetSocketAddress local2 = InetSocketAddress (Ipv4Address::GetAny(), 9);
   Ptr<Socket> recvSinkNewLans[nWifi];
     for (int i=1; i<nWifi; i++)
  {
    recvSinkNewLans[i] =  Socket::CreateSocket (newLanNodes[i].Get (0), tid);
    recvSinkNewLans[i]->Bind (local2);
    recvSinkNewLans[i]->SetRecvCallback (MakeCallback (&ReceivePacketNewLans));
  }

  
  
  p2pDevices[0].Get(0)->SetPromiscReceiveCallback(MakeCallback(&callback_promisc)); 
  Simulator::Schedule(Seconds(2.5), &manipulate_packet);

  Time interPacketInterval = Seconds (packetInterval);
  // Transmitter socket connections. Set transmitter for broadcasting
  InetSocketAddress *remote[nWifi];
   for(int i=1; i<nWifi; i++)
   {
     remote[i]= new InetSocketAddress(p2p_address.GetAddress(i), 10);
     source[i]->SetAllowBroadcast (true); 
     source[i]->Connect (*remote[i]);

     Simulator::ScheduleWithContext (source[i]->GetNode ()->GetId (),
                                  Seconds (2.0), &GenerateTraffic, 
                                  source[i], packetSize, packetCount, interPacketInterval);
   }


  
Packet::EnablePrinting();  


//----------------------------------------------------------------------
/// Generic Routing Tables.

 Ipv4GlobalRoutingHelper::PopulateRoutingTables ();
 

 for(int i=1; i<nWifi; i++)
 {

  struct table t;
  t.myNode=wifiStaNodes.Get(i);
  t.myID=wifiStaNodes.Get(i)->GetId();
  t.remoteID=newLanNodes[i].Get(0)->GetId();
  t.myIP=mitm_lan_address.GetAddress(i);
  t.remoteIP=p2p_address.GetAddress(i);
  v_table.push_back(t);
 }


  

   // Trace routing tables
   Ipv4GlobalRoutingHelper g;
   Ptr<OutputStreamWrapper> routingStream = Create<OutputStreamWrapper>
("RoutingTable", std::ios::out);
   g.PrintRoutingTableAllAt (Seconds (12), routingStream);

/// Flow Monitor processing.
   Ptr<FlowMonitor> flowMonitor;
   FlowMonitorHelper flowHelper;
   flowMonitor = flowHelper.InstallAll();

  
  
  
 
   Simulator::Stop (Seconds (3.0));


/// Wireshark trace file.
  if (tracing == true)
    {
      phy.EnablePcap ("simulacion2_pcap", staDevices);

    }

/// Enable animation with Netanim.
  AnimationInterface anim ("animacion2.xml");


/// Enable metadata, very useful to represent in Netanim.
  anim.EnablePacketMetadata (true);

  Simulator::Run ();
  Packet::EnablePrinting();


 

  Gnuplot2dDataset dataset;
    
  //Flow Monitor ... continued

 
    double Throughput=0.0;

    flowMonitor->CheckForLostPackets ();
    Ptr<Ipv4FlowClassifier> classifier = DynamicCast<Ipv4FlowClassifier> (flowHelper.GetClassifier ());
    std::map<FlowId, FlowMonitor::FlowStats> stats = flowMonitor->GetFlowStats ();

    for (std::map<FlowId, FlowMonitor::FlowStats>::const_iterator iter = stats.begin (); iter != stats.end (); ++iter)
      {
      /*Ipv4FlowClassifier::FiveTuple t = classifier->FindFlow (iter->first);

        NS_LOG_UNCOND("Flow ID: " << iter->first << " Src Addr " << t.sourceAddress << " Dst Addr " << t.destinationAddress);
        NS_LOG_UNCOND("Tx Packets = " << iter->second.txPackets);
        NS_LOG_UNCOND("Rx Packets = " << iter->second.rxPackets);
        NS_LOG_UNCOND("Delay Sum = " << iter->second.delaySum);
        NS_LOG_UNCOND("Delay = " << iter->second.delaySum / iter->second.rxPackets << "ns");
        NS_LOG_UNCOND("Delay = " << (iter->second.delaySum / iter->second.rxPackets)/1000000 << "ms");
        NS_LOG_UNCOND("Jitter Sum = " << iter->second.jitterSum);
        NS_LOG_UNCOND("Jitter = " << iter->second.jitterSum / (iter->second.rxPackets - 1) << "ns");
        //NS_LOG_UNCOND("Jitter = " << (iter->second.jitterSum / (iter->second.rxPackets - 1))/1000000 << "ms");
        NS_LOG_UNCOND("Throughput: " << iter->second.rxBytes * 8.0 / (iter->second.timeLastRxPacket.GetSeconds()-iter->second.timeFirstTxPacket.GetSeconds()) / 1024  << " Kbps");*/
        Throughput=iter->second.rxBytes * 8.0 /
                  (iter->second.timeLastRxPacket.GetSeconds()-iter->second.timeFirstTxPacket.GetSeconds())
                  / 1024;

   
        dataset.Add((double)iter->first,(double) Throughput);
            
       /* NS_LOG_UNCOND("Lost Packets = " << iter->second.lostPackets);
        NS_LOG_UNCOND("-------------------------------------------------");
        NS_LOG_UNCOND("-------------------------------------------------");*/

      }


  //Gnuplot
  plotGraph("Flow vs Throughput", "Flow", "Throughput",dataset);

    

  flowMonitor->SerializeToXmlFile("monitor.flowmonitor2", true, true);
 



  Simulator::Destroy ();
  cout<<"       ----Stats----      "<<endl;
  cout<<"Recv Paquetes nodo 6: "<<cont1<<endl;
  cout<<"Recv Paquetes nodo 7: "<<cont2<<endl;
  cout<<"Recv Paquetes nodo 8: "<<cont3<<endl;
  cout<<"Sent Packet MITM End Nodes: "<< cont_socketMITM_end << endl;
  cout<< "Total Recv packets at MiTM: " << cont_mitm << endl;
  cout<< "Total Recv packets at End Devices: " << cont_global << endl;
  cout<< "Total Send packets at RC: " << cont_send << endl;

  cout<<"----MITM relationship table----"<<endl;
for(int i=0;i<(int)v_table.size();i++)
 {
  cout<<"MyID: " <<v_table[i].myID << " | " << "RemoteID: " <<v_table[i].remoteID << " | " <<"MyIP: " <<v_table[i].myIP << " | " <<"Remote IP: " <<v_table[i].remoteIP << endl;
 }

/*  for(int i=0; i<(int)packetCount; ++i)
  {

    cout<<"----E2E Delay: "<< finish[i]-start[i]<<endl;
    
  }*/


  return 0;
}
