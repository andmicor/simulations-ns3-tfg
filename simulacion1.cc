/*! \file simulacion1.cc
  \brief First Simulation. Full wireless network, trying to analyse congestions and crashes. NS3 libraries are needed to simulate the topology, including wifi-module, the 
  NS3 GUI Netanim and the Flow Monitor. We are trying to represent a Multicast frame topology and calculating congestion using report files generated.
*/

#include <iostream>
#include <unistd.h>
#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/applications-module.h"
#include "ns3/wifi-module.h"
#include "ns3/mobility-module.h"
#include "ns3/csma-module.h"
#include "ns3/internet-module.h"
#include "ns3/netanim-module.h"
#include "ns3/flow-monitor-module.h"



/// Default Network Topology
//
///  Wifi 192.168.1.0
///                      AP
///  *    *    *         *
///  |    |    |         |    
/// n0   n1   n2  .....   n4
///                   
//                               



using namespace ns3;
using namespace std;


NS_LOG_COMPONENT_DEFINE ("Simulacion1");

int 
main (int argc, char *argv[])
{
  bool verbose = true;/// Program log show in the output 
  int nWifi = 18; /// Number of nodes in our topology 
  bool tracing = false; /// For Wireshark representation
  double rss=-30; /// Received signal in dB from AP 

  CommandLine cmd;
  cmd.AddValue ("nWifi", "Number of wifi STA devices", nWifi);
  cmd.AddValue ("verbose", "Tell echo applications to log if true", verbose);
  cmd.AddValue ("tracing", "Enable pcap tracing", tracing);

  cmd.Parse (argc,argv);

  /*! \brief The underlying restriction of 18 is due to the grid position
   allocator's configuration; the grid layout will exceed the
  bounding box if more than 18 nodes are provided.
  */
  if (nWifi > 18)
    {
      std::cout << "nWifi should be 18 or less; otherwise grid layout exceeds the bounding box" << std::endl;
      return 1;
    }

  if (verbose)
    {
      LogComponentEnable ("UdpEchoClientApplication", LOG_LEVEL_INFO);
      LogComponentEnable ("UdpEchoServerApplication", LOG_LEVEL_INFO);
    }

///Setting Wireless nodes 
  NodeContainer wifiStaNodes; /// Statics 
  wifiStaNodes.Create (nWifi); 
  NodeContainer wifiApNode;
  wifiApNode.Create(1);/// To route all packets, an AP node is generated  

 

/// Medium is configured. IEEE 802.11. 
  YansWifiChannelHelper channel = YansWifiChannelHelper::Default (); /// We set the default channel. 
  YansWifiPhyHelper phy = YansWifiPhyHelper::Default ();
  phy.SetChannel (channel.Create ()); /// Channel is created. 

/*! \brief This is one parameter that matters when using FixedRssLossModel
   set it to zero; otherwise, gain will be added */
  phy.Set ("RxGain", DoubleValue (0));

  /// ns-3 supports RadioTap and Prism tracing extensions for 802.11g
  phy.SetPcapDataLinkType (YansWifiPhyHelper::DLT_IEEE802_11_RADIO);

/// ns-3 supports RadioTap and Prism tracing extensions for 802.11g
  channel.SetPropagationDelay ("ns3::ConstantSpeedPropagationDelayModel");

  /*! \brief The below FixedRssLossModel will cause the rss to be fixed regardless
   of the distance between the two stations, and the transmit power*/
  channel.AddPropagationLoss ("ns3::FixedRssLossModel","Rss",
    DoubleValue (rss));
  phy.SetChannel (channel.Create ());


/// Configuring WiFi
    WifiHelper wifi;

  wifi.SetRemoteStationManager ("ns3::AarfWifiManager");

/// Setting both Level two layer in AP and static nodes
  WifiMacHelper mac;
  Ssid ssid = Ssid ("ns-3-ssid");
  mac.SetType ("ns3::StaWifiMac",
               "Ssid", SsidValue (ssid),
               "ActiveProbing", BooleanValue (false));

/// Installing static devices
  NetDeviceContainer staDevices;
  staDevices = wifi.Install (phy, mac, wifiStaNodes);


 /// AP device 
  mac.SetType ("ns3::ApWifiMac",
               "Ssid", SsidValue (ssid));

/// Installing AP devices
  NetDeviceContainer apDevices;
  apDevices = wifi.Install (phy, mac, wifiApNode);


/// Setting a mobility model in static nodes
  MobilityHelper mobility;

  mobility.SetPositionAllocator ("ns3::GridPositionAllocator",
                                 "MinX", DoubleValue (0.0),
                                 "MinY", DoubleValue (0.0),
                                 "DeltaX", DoubleValue (5.0),
                                 "DeltaY", DoubleValue (10.0),
                                 "GridWidth", UintegerValue (3),
                                 "LayoutType", StringValue ("RowFirst"));
/// Setting a mobility model in static nodes
  mobility.SetMobilityModel ("ns3::RandomWalk2dMobilityModel",
                             "Bounds", RectangleValue (Rectangle (-50, 50, -50, 50)));
  mobility.Install (wifiStaNodes);

/// AP is set as immovable
  mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
  mobility.Install (wifiApNode);

/// Internet stack is needed to be installed in both kind of nodes
  InternetStackHelper stack;
  stack.Install (wifiApNode);
  stack.Install (wifiStaNodes);



  Ipv4AddressHelper address;

/// Setting Adressing.
  address.SetBase ("192.168.1.0", "255.255.255.0");
 Ipv4InterfaceContainer sta_address = address.Assign (staDevices);
 Ipv4InterfaceContainer ap_address =  address.Assign (apDevices);
// --------------------------------------------------------------------


  

  
/*! \brief Configuring a Server-Client topology. We need a Client and Server vector to expand and parameterize the simulation.*/
  ApplicationContainer ServerApps[nWifi];

  UdpEchoServerHelper * server[nWifi];

/// Server is waiting UDP packets from Client in node 1 to nWifi. Port 9 is assigned. Servers start at 1s and stop at 10s.
  for (int i=1; i<nWifi; i++)
  {
    server[i] = new UdpEchoServerHelper(9);
    ServerApps[i]=server[i]->Install(wifiStaNodes.Get(i));
    ServerApps[i].Start (Seconds (1.0));
    ServerApps[i].Stop (Seconds (10.0));
  }

/// A client is installed at node 0 and sending request to servers. Client start at 2s and stops at 10
  ApplicationContainer ClientApps[nWifi];
  UdpEchoClientHelper * client[nWifi];

  for (int i=1 ; i<nWifi; i++)
  {
   
     client[i]= new UdpEchoClientHelper(sta_address.GetAddress(i), 9);
    client[i]-> SetAttribute ("MaxPackets", UintegerValue (10000));
    client[i]->SetAttribute ("Interval", TimeValue (Seconds (0.1)));
    client[i]->SetAttribute ("PacketSize", UintegerValue (1024));

    ClientApps[i]=client[i]->Install(wifiStaNodes.Get(0));
    ClientApps[i].Start (Seconds (2.0));
    ClientApps[i].Stop (Seconds (10.0));
    
  }


//----------------------------------------------------------------------
/// Generic Routing Tables.
  Ipv4GlobalRoutingHelper::PopulateRoutingTables ();

/// Flow Monitor processing.
  Ptr<FlowMonitor> flowMonitor;
  FlowMonitorHelper flowHelper;
  flowMonitor = flowHelper.InstallAll();
  Simulator::Stop (Seconds (20.0));

/// Wireshark trace file.
  if (tracing == true)
    {
      phy.EnablePcap ("simulacion", apDevices.Get (0));
    }

/// Enable animation with Netanim.
  AnimationInterface anim ("animacion1.xml");

/// Enable metadata, very useful to represent in Netanim.
  anim.EnablePacketMetadata (true);

  Simulator::Run ();
  flowMonitor->SerializeToXmlFile("monitor.flowmonitor1", true, true);
  



  Simulator::Destroy ();
  return 0;
}
