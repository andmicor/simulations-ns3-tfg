#ifndef FUNCTIONS_H
#define FUNCTIONS_H


using namespace ns3;
using namespace std;

  int nWifi = 10; /// Number of nodes in our topology 

//Table of linked End-Devices Nodes
 struct table
 {
  Ptr<Node> myNode;
  uint32_t myID;
  Ipv4Address myIP;
 };

struct stats_time
 {
    uint64_t pkt_id;
    int64_t send_time;
    int64_t recv_time;
    int64_t e2e_delay;
    Ipv4Address destination;
 };


 class MatchesID
{
    uint64_t _ID;

public:
    MatchesID(const uint64_t ID) : _ID(ID) {}

    bool operator()(const struct stats_time *item) const
    {
        return item->pkt_id == _ID;
    }
};

 // vector containing times and relationships between packets.
  vector<stats_time *> v_time;

  //Vector containing conections between End-Devices nodes
  vector<table> v_table;

  struct stats_time *t;



// Receive method in each End Device
void ReceivePacket (Ptr<Socket> socket)
{
    Ptr<Packet> packet; 
    uint64_t id_unicast;


    packet = socket->Recv();
    id_unicast=packet->GetUid();
    cout<<"----Recv time: " <<Simulator::Now().GetMilliSeconds()<<endl;
    
    vector<stats_time*>::iterator it = find_if( v_time.begin(), v_time.end(), MatchesID(id_unicast));
    (*it)->recv_time=Simulator::Now().GetMicroSeconds();
    (*it)->e2e_delay=((*it)->recv_time)-((*it)->send_time);

    for(int i=0; i<(int) v_table.size();i++)
    {
    	if(socket->GetNode()==v_table[i].myNode)
    		    (*it)->destination=v_table[i].myIP;


    }

    cout <<"Se recibe un paquete en el End Device: "<<socket->GetNode()->GetId()<<endl;
      


}



//Generate traffic on RC
static void GenerateTraffic (Ptr<Socket> socket, uint32_t pktSize, 
                             uint32_t pktCount, Time pktInterval )
{
   
  if (pktCount > 0)
    {
      cout<<"++++Send time: " << Simulator::Now().GetMilliSeconds()<<endl;
     
      
      t=new struct stats_time;
     
      Ptr<Packet> packet= Create<Packet> (pktSize);
      
      t->pkt_id=packet->GetUid();
      t->send_time=Simulator::Now().GetMicroSeconds();

      socket->Send(packet);

      Simulator::Schedule (pktInterval, &GenerateTraffic, 
                           socket, pktSize,pktCount-1, pktInterval);

  
      v_time.push_back(t);
      
    }

  else
    {
      socket->Close ();
    }
}

#endif