/*! \file simulacion1.cc
  \brief First Simulation. Full wireless network, trying to analyse congestions and crashes. NS3 libraries are needed to simulate the topology, including wifi-module, the 
  NS3 GUI Netanim and the Flow Monitor. We are trying to represent a Multicast frame topology and calculating congestion using report files generated.
*/

#include <iostream>
#include <vector>
#include <algorithm>

#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/applications-module.h"
#include "ns3/wifi-module.h"
#include "ns3/mobility-module.h"
#include "ns3/csma-module.h"
#include "ns3/internet-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/netanim-module.h"
#include "ns3/flow-monitor-module.h"
#include "ns3/node-container.h"
#include "ns3/node.h"
#include "ns3/address.h"
#include "ns3/net-device.h"
#include "ns3/inet-socket-address.h"
#include "ns3/gnuplot.h"
#include "ns3/stats-module.h"


#include "functions.h"



/// Default Network Topology
//
///  Wifi 192.168.1.0
///                      AP
///  *    *    *         *
///  |    |    |         |    
/// n0   n1   n2  .....   n4
///                   
//                               




NS_LOG_COMPONENT_DEFINE ("Simulacion1");

int 
main (int argc, char *argv[])
{
  bool verbose = true;/// Program log show in the output 
  bool tracing = false; /// For Wireshark representation
  double rss=-30; /// Received signal in dB from AP 

  uint32_t packetSize = 1024;
  uint32_t packetCount = 100;
  double packetInterval = 0.1;

  Time start_simulation=Seconds(2.0);
  Time stop_simulation=Seconds(3.0);

  CommandLine cmd;
  cmd.AddValue ("nWifi", "Number of wifi STA devices", nWifi);
  cmd.AddValue ("verbose", "Tell echo applications to log if true", verbose);
  cmd.AddValue ("tracing", "Enable pcap tracing", tracing);

  cmd.Parse (argc,argv);

  /*! \brief The underlying restriction of 18 is due to the grid position
   allocator's configuration; the grid layout will exceed the
  bounding box if more than 18 nodes are provided.
  */
  if (nWifi > 18)
    {
      std::cout << "nWifi should be 18 or less; otherwise grid layout exceeds the bounding box" << std::endl;
      return 1;
    }

  if (verbose)
    {
      LogComponentEnable ("UdpEchoClientApplication", LOG_LEVEL_INFO);
      LogComponentEnable ("UdpEchoServerApplication", LOG_LEVEL_INFO);
    }

///Setting Wireless nodes 
  NodeContainer wifiStaNodes; /// Statics 
  wifiStaNodes.Create (nWifi); 
  NodeContainer wifiApNode;
  wifiApNode.Create(1);/// To route all packets, an AP node is generated  

 

/// Medium is configured. IEEE 802.11. 
  YansWifiChannelHelper channel = YansWifiChannelHelper::Default (); /// We set the default channel. 
  YansWifiPhyHelper phy = YansWifiPhyHelper::Default ();
  phy.SetChannel (channel.Create ()); /// Channel is created. 

/*! \brief This is one parameter that matters when using FixedRssLossModel
   set it to zero; otherwise, gain will be added */
  phy.Set ("RxGain", DoubleValue (0));

  /// ns-3 supports RadioTap and Prism tracing extensions for 802.11g
  phy.SetPcapDataLinkType (YansWifiPhyHelper::DLT_IEEE802_11_RADIO);

/// ns-3 supports RadioTap and Prism tracing extensions for 802.11g
  channel.SetPropagationDelay ("ns3::ConstantSpeedPropagationDelayModel");

  /*! \brief The below FixedRssLossModel will cause the rss to be fixed regardless
   of the distance between the two stations, and the transmit power*/
  channel.AddPropagationLoss ("ns3::FixedRssLossModel","Rss",
    DoubleValue (rss));
  phy.SetChannel (channel.Create ());


/// Configuring WiFi
    WifiHelper wifi;

  wifi.SetRemoteStationManager ("ns3::AarfWifiManager");

/// Setting both Level two layer in AP and static nodes
  WifiMacHelper mac;
  Ssid ssid = Ssid ("ns-3-ssid");
  mac.SetType ("ns3::StaWifiMac",
               "Ssid", SsidValue (ssid),
               "ActiveProbing", BooleanValue (false));

/// Installing static devices
  NetDeviceContainer staDevices;
  staDevices = wifi.Install (phy, mac, wifiStaNodes);


 /// AP device 
  mac.SetType ("ns3::ApWifiMac",
               "Ssid", SsidValue (ssid));

/// Installing AP devices
  NetDeviceContainer apDevices;
  apDevices = wifi.Install (phy, mac, wifiApNode);


/// Setting a mobility model in static nodes
  MobilityHelper mobility;

  mobility.SetPositionAllocator ("ns3::GridPositionAllocator",
                                 "MinX", DoubleValue (0.0),
                                 "MinY", DoubleValue (0.0),
                                 "DeltaX", DoubleValue (5.0),
                                 "DeltaY", DoubleValue (10.0),
                                 "GridWidth", UintegerValue (3),
                                 "LayoutType", StringValue ("RowFirst"));
/// Setting a mobility model in static nodes
  mobility.SetMobilityModel ("ns3::RandomWalk2dMobilityModel",
                             "Bounds", RectangleValue (Rectangle (-50, 50, -50, 50)));
  mobility.Install (wifiStaNodes);

/// AP is set as immovable
  mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
  mobility.Install (wifiApNode);

/// Internet stack is needed to be installed in both kind of nodes
  InternetStackHelper stack;
  stack.Install (wifiApNode);
  stack.Install (wifiStaNodes);



  Ipv4AddressHelper address;

/// Setting Adressing.
  address.SetBase ("192.168.1.0", "255.255.255.0");
 Ipv4InterfaceContainer sta_address = address.Assign (staDevices);
 Ipv4InterfaceContainer ap_address =  address.Assign (apDevices);
// --------------------------------------------------------------------

 //Creating End Devices relationship table
 for(int i=0; i<nWifi; i++)
 {

  struct table t;
  t.myNode=wifiStaNodes.Get(i);
  t.myID=wifiStaNodes.Get(i)->GetId();
  t.myIP=sta_address.GetAddress(i);
 
  v_table.push_back(t);
 }

  //Sockets
  NS_LOG_INFO ("Create sockets.");
  //Receiver socket on n1
  TypeId tid = TypeId::LookupByName ("ns3::UdpSocketFactory");

  //Sender socket on n0
  Ptr<Socket> source[nWifi];
  for(int i=0; i<nWifi; i++)
    source[i] =  Socket::CreateSocket (wifiStaNodes.Get(0), tid);


//  Receiver sockets
  Ptr<Socket> recvSink[nWifi];

  InetSocketAddress local = InetSocketAddress (Ipv4Address::GetAny(), 10);

  for(int i=1;i<nWifi; i++)
  {
    recvSink[i] =  Socket::CreateSocket (wifiStaNodes.Get (i), tid);
    recvSink[i]->Bind (local);
    recvSink[i]->SetRecvCallback (MakeCallback (&ReceivePacket));
  }
  
 

  Time interPacketInterval = Seconds (packetInterval);
  // Transmitter socket connections. Set transmitter for broadcasting
  InetSocketAddress *remote[nWifi];
   for(int i=1; i<nWifi; i++)
   {
     remote[i]= new InetSocketAddress(sta_address.GetAddress(i), 10);
     source[i]->Connect (*remote[i]);

     Simulator::ScheduleWithContext (source[i]->GetNode ()->GetId (),
                                  start_simulation, &GenerateTraffic, 
                                  source[i], packetSize, packetCount, interPacketInterval);
   }


  
  Packet::EnablePrinting();  


//----------------------------------------------------------------------
/// Generic Routing Tables.
  Ipv4GlobalRoutingHelper::PopulateRoutingTables ();




/// Flow Monitor processing.
  Ptr<FlowMonitor> flowMonitor;
  FlowMonitorHelper flowHelper;
  flowMonitor = flowHelper.InstallAll();
  Simulator::Stop (stop_simulation);

/// Wireshark trace file.
  if (tracing == true)
    {
      phy.EnablePcap ("simulacion", apDevices.Get (0));
    }

/// Enable animation with Netanim.
  AnimationInterface anim ("animacion1.xml");

/// Enable metadata, very useful to represent in Netanim.
  anim.EnablePacketMetadata (true);

  Simulator::Run ();
  flowMonitor->SerializeToXmlFile("monitor.flowmonitor1", true, true);
  
 


  Simulator::Destroy ();

  ofstream csvfile;
  csvfile.open ("stats_simulation1.csv");

  for(int i=0; i<(int) v_time.size(); i++)
  {
    csvfile<<v_time[i]->destination<<","<<v_time[i]->pkt_id << ","  <<
    v_time[i]->send_time <<"," <<v_time[i]->recv_time << 
    "," <<v_time[i]->e2e_delay<<endl ;
  }
  csvfile.close();


   for(int i=0;i<(int)v_table.size();i++)
  {
    cout<<"MyID: " <<v_table[i].myID << " | "  <<"MyIP: " <<v_table[i].myIP << endl;
  }
  cout<<endl<<endl;

   for(int i=0; i<(int) v_time.size(); i++)
  {
    cout<<"Destination IP: " << v_time[i]->destination<<" | "<<"Pkt ID: " <<v_time[i]->pkt_id << " | " <<"Send time: " <<
    v_time[i]->send_time << " | " <<"Recv Time: " <<v_time[i]->recv_time << 
    " | " <<"E2E Delay: " <<v_time[i]->e2e_delay <<endl ;
  }
  return 0;
}
